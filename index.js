const Web3 = require("web3");
const abi = require("./hex.json");
const config = require("./config.json");
const keystore = require("./keystore.json");
const prompt = require('prompt');

const HEXADDR = "0x2b591e99afE9f32eAA6214f7B7629768c40Eeb39";
//var web3 = new Web3('wss://mainnet.infura.io/ws/v3/0396b23ac37049f0b354524953516a43');
const web3 = new Web3("wss://ws.v2b.testnet.pulsechain.com");

const hex = new web3.eth.Contract(abi,HEXADDR);
let decryptedaccount;


prompt.start();
prompt.get([{name:'PassWord', required: true}], async function (err, result) {

    decryptedaccount = web3.eth.accounts.decrypt(keystore, result.PassWord);
    web3.eth.accounts.wallet.add(decryptedaccount);

    console.log("Wallet", decryptedaccount.address);
    console.log("Listenig...");


    hex.events.Transfer({filter:{ 
        fromBlock: "finalized",
        to: decryptedaccount.address
    }}, async (err,evt) => {

        if(err) {
            console.log(err)
        }
        else {
            console.log("received",evt.returnValues.value/1e8,"HEX")

            const data1 = hex.methods.transfer(config.destination_wallet,evt.returnValues.value);
            let nonce = await web3.eth.getTransactionCount(decryptedaccount.address);

            send("transfer",{
                nonce: nonce,
                from: decryptedaccount.address,
                to: HEXADDR,
                data: data1.encodeABI()
            });
        }
    });
});


async function send(tag,tx) {

    const gas = await web3.eth.estimateGas(tx);
    tx.gasLimit = web3.utils.toHex(gas);
    tx.chainId= 941;

    const gasPrice = await web3.eth.getGasPrice();
    tx.gasPrice= Web3.utils.toHex(gasPrice*config.gas_factor);
    
    //console.log(tx);

    const signedTx = await decryptedaccount.signTransaction(tx);
    console.log(signedTx);

    web3.eth.sendSignedTransaction(signedTx.rawTransaction)
    .once('transactionHash', async hash => {
        console.log(tag,hash);
    })
    .once('confirmation', async (c, receipt) => {
        console.log(tag,"confirmed");
    })
    .once('error', async err => {
        console.log(tag,err.message);
    })
}