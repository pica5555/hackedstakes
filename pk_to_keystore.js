const fs = require("fs");
const Web3 = require("web3");
const prompt = require('prompt');

const web3 = new Web3();

prompt.start();
prompt.get([{name:'PK', required:true},{name:'PassWord'}], async function (err, result) {

    const value = await web3.eth.accounts.encrypt(result.PK, result.PassWord);
    fs.writeFileSync("./keystore.json", JSON.stringify(value));
    console.log('✅');
});